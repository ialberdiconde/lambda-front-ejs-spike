# AWS Lambda + EJS

This is an example of a lambda function written in NODE JS to render a Web. 

It renders front end using EJS templates 

## Running it

```bash
npm install
npm bin/render.js > index.html
```

## Deployment on AWS Lambda

To deploy on aws lambda you have to use AWS-CLI:

```
zip -r spike.zip ./*
aws lambda update-function-code --function-name FUNCTION_NAME --zip-file fileb://spike.zip
rm spike.zip
```

Make sure your aws user has correct rights. Here is an IAM policy which you can use or attach all exec lambda policy to your user:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "lambda:UpdateFunctionCode",
                "lambda:UpdateFunctionConfiguration", 
                "lambda:InvokeFunction",
                "lambda:GetFunction"
            ],
            "Resource": ["ARN_ADDRESS_OF_YOUR_LAMBDA_FUNCTION"]
        }
    ]
}

```

To test uploaded function run:

```
aws lambda invoke --function-name FUNCTION_NAME index.html
cat index.html
```
I created a user ivan_alberdi and I attach all possible permissions to work with lambdas, so if you want to

re configure your creds (saved yours somewhere) and asked me via DM (slack) and I will provide keys and region

Otherwise just execute the following URL to see how it displays the simple HTML

https://68eaiuhet5.execute-api.us-east-1.amazonaws.com/dev
