const ejs = require('ejs');
const moment = require('moment');
const path = require('path');

class Renderer {
  constructor(params) {
    this.ejsPath = path.join(process.cwd()+'/src/views/index.ejs');;
    this.params = params || {};
  }

  static helpers() {
    return { moment };
  }

  async render() {
    const params = {
      ...this.params,
      helpers: Renderer.helpers()
    }

    const templateStr = ejs.fileLoader(this.ejsPath, 'utf8');
    const template = ejs.compile(templateStr, { filename: this.ejsPath });
    return template(params);
  }
}

module.exports = Renderer
